exports.definition = {
	config: {
		columns: {
		    "nome": "string",
		    "cognome": "string",
		    "email": "string",
		    "datanascita": "date",
		   /* "sesso": "string",
		    "attivo": "string" */
		},
		adapter: {
			type: "sql",
			collection_name: "Contact"
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});

		return Model;
	},
	extendCollection: function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
		});

		return Collection;
	}
};