exports.definition = {
    config: {
        columns: {
            nome: "string",
            cognome: "string",
            email: "string",
            datanascita: "date"
        },
        adapter: {
            type: "sql",
            collection_name: "Contact"
        }
    },
    extendModel: function(Model) {
        _.extend(Model.prototype, {});
        return Model;
    },
    extendCollection: function(Collection) {
        _.extend(Collection.prototype, {});
        return Collection;
    }
};

var Alloy = require("alloy"), _ = require("alloy/underscore")._, model, collection;

model = Alloy.M("Contact", exports.definition, []);

collection = Alloy.C("Contact", exports.definition, model);

exports.Model = model;

exports.Collection = collection;