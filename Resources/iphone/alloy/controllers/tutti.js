function Controller() {
    function __alloyId15(e) {
        if (e && e.fromAdapter) return;
        __alloyId15.opts || {};
        var models = __alloyId14.models;
        var len = models.length;
        var rows = [];
        for (var i = 0; len > i; i++) {
            var __alloyId11 = models[i];
            __alloyId11.__transform = {};
            var __alloyId13 = Ti.UI.createTableViewRow({
                title: "undefined" != typeof __alloyId11.__transform["nome"] ? __alloyId11.__transform["nome"] : __alloyId11.get("nome")
            });
            rows.push(__alloyId13);
        }
        $.__views.__alloyId10.setData(rows);
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "tutti";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    Alloy.Collections.instance("contact");
    $.__views.tutti = Ti.UI.createView({
        id: "tutti"
    });
    $.__views.tutti && $.addTopLevelView($.__views.tutti);
    $.__views.__alloyId10 = Ti.UI.createTableView({
        id: "__alloyId10"
    });
    $.__views.tutti.add($.__views.__alloyId10);
    var __alloyId14 = Alloy.Collections["contact"] || contact;
    __alloyId14.on("fetch destroy change add remove reset", __alloyId15);
    exports.destroy = function() {
        __alloyId14.off("fetch destroy change add remove reset", __alloyId15);
    };
    _.extend($, $.__views);
    arguments[0] || {};
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;