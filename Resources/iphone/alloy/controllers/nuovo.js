function Controller() {
    function salva() {
        var contact = Alloy.createModel("contact");
        contact.set("nome", $.nome.value);
        contact.set("cognome", $.cognome.value);
        contact.set("email", $.email.value);
        contact.set("datanascita", $.datanascita.value);
        contact.save();
        Alloy.Collections.contact.fetch();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "nuovo";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    var __defers = {};
    $.__views.nuovo = Ti.UI.createView({
        layout: "vertical",
        id: "nuovo"
    });
    $.__views.nuovo && $.addTopLevelView($.__views.nuovo);
    $.__views.nome = Ti.UI.createTextField({
        height: "30",
        width: "90%",
        borderWidth: 1,
        id: "nome"
    });
    $.__views.nuovo.add($.__views.nome);
    $.__views.cognome = Ti.UI.createTextField({
        height: "30",
        width: "90%",
        borderWidth: 1,
        id: "cognome"
    });
    $.__views.nuovo.add($.__views.cognome);
    $.__views.email = Ti.UI.createTextField({
        height: "30",
        width: "90%",
        borderWidth: 1,
        id: "email"
    });
    $.__views.nuovo.add($.__views.email);
    $.__views.datanascita = Ti.UI.createPicker({
        id: "datanascita",
        type: Ti.UI.PICKER_TYPE_DATE
    });
    $.__views.nuovo.add($.__views.datanascita);
    $.__views.attivo = Ti.UI.createSwitch({
        value: false,
        id: "attivo"
    });
    $.__views.nuovo.add($.__views.attivo);
    $.__views.btSalva = Ti.UI.createButton({
        height: "30",
        width: "90%",
        borderWidth: 1,
        id: "btSalva",
        title: "salva"
    });
    $.__views.nuovo.add($.__views.btSalva);
    salva ? $.__views.btSalva.addEventListener("click", salva) : __defers["$.__views.btSalva!click!salva"] = true;
    exports.destroy = function() {};
    _.extend($, $.__views);
    arguments[0] || {};
    __defers["$.__views.btSalva!click!salva"] && $.__views.btSalva.addEventListener("click", salva);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;